FROM golang:1.19-alpine as build
RUN apk --no-cache add tzdata
WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY cmd/*.go ./

RUN CGO_ENABLED=0 go build -o fronius2mqtt .

FROM scratch as final
COPY --from=build /app/fronius2mqtt .
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
ENV TZ=Europe/Berlin
CMD ["/fronius2mqtt"]
