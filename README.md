# fronius2mqtt

![](https://img.shields.io/gitlab/last-commit/42618752?gitlab_url=https%3A%2F%2Fgitlab.com)
![Last tag](https://img.shields.io/gitlab/v/tag/42618752?sort=semver&color=green)
![Platform](https://img.shields.io/static/v1?label=platform&message=amd64%20%2f%20arm64%20%2f%20arm&color=red)
<img src="logo.png" width="75" align="right">

## Description

Collect data from Fronius PV Inverter from local network and send it to MQTT Broker. Currently only Symo Inverters are supported but feel free to ask for other types. Please have in mind that I am not a professional developer. If you have any proposal please feel free to open an issue, I am eager to learn. ;)

## Getting started

* Clone repo
* Rename config.sample.yml to config.yml
* Fill/adjust parameters for your needs
* build using Makefile "make" in root directory
* run bin/*your_arch*/fronius2mqtt (-d for debugging)

### Docker

```bash
docker run --rm -v "$(pwd)"/config.yml:/config.yml:ro registry.gitlab.com/cooling75/fronius2mqtt/fronius2mqtt-docker:v1.2.1
```

### Docker-Compose

```yaml
version: "3"

services:

  fronius:
    container_name: fronius2mqtt
    image: registry.gitlab.com/cooling75/fronius2mqtt/fronius2mqtt-docker:v1.2.1
    restart: on-failure
    volumes:
      - ./config.yml:/config.yml:ro

```

### Kubernetes

I run a cluster of raspberry pis with K3s at home and use this [manifest](manifest.yml).

## Installation

See [Getting started](#getting-started) 

## Roadmap

- [x] Add MQTT authentication
- [x] Add multiarch docker images
- [ ] Build Helm chart (will take a while)

## License

See [LICENSE](LICENSE)

## Project status

Ongoing

## Author

cooling75 / January 2023
