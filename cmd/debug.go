package main

import "fmt"

func debugConfig(c conf) {
	fmt.Println("")
	fmt.Println("########## DEBUG ##########")
	println(fmt.Sprintf("PV Inverter IP: %s", c.InverterIP))
	println(fmt.Sprintf("Query interval (minutes): %d", c.QueryInterval))
	// println(fmt.Sprintf("Timezone: %s", c.Timezone))
	println(fmt.Sprintf("Latitude: %f", c.Latitude))
	println(fmt.Sprintf("Longitude: %f", c.Longitude))
	println(fmt.Sprintf("MQTT broker: %s", c.MqttBroker))
	println(fmt.Sprintf("MQTT port: %d", c.MqttPort))
	println(fmt.Sprintf("MQTT topic: %s", c.MqttTopic))
	fmt.Println("###########################")
	fmt.Println("")
}
