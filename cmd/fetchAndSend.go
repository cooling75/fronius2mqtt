package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func callAPI(url string) string {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalln(err.Error())
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err.Error())
		log.Println("No data probably due to night time shutdown or simply darkness, exiting job.")
		return "nighttime"
	}

	defer res.Body.Close()
	body, readErr := io.ReadAll(res.Body)
	if readErr != nil {
		log.Fatalln(readErr.Error())
	}

	return (string(body))
}

func fetchAndSend(c conf, mqttClient mqtt.Client) {

	var commonStruct Common
	var threePhaseStruct ThreePhase

	// API query URLs
	var urlCommon = "http://" + c.InverterIP + "/solar_api/v1/GetInverterRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=CommonInverterData"
	var urlThreePhase = "http://" + c.InverterIP + "/solar_api/v1/GetInverterRealtimeData.cgi?Scope=Device&DeviceId=1&DataCollection=3PInverterData"

	res := callAPI(urlCommon)
	if res == "nighttime" {
		fmt.Println("Night time shutdown detected. Exiting scheduled job.")
		return
	}
	if len(os.Args[1:]) > 0 && os.Args[1] == "-d" {
		fmt.Println(res)
	}
	err := json.Unmarshal([]byte(res), &commonStruct)
	if err != nil {
		log.Fatalln("Could not unmarshal json", err)
	}

	res = callAPI(urlThreePhase)
	if len(os.Args[1:]) > 0 && os.Args[1] == "-d" {
		fmt.Println(res)
	}
	err = json.Unmarshal([]byte(res), &threePhaseStruct)
	if err != nil {
		log.Fatalln("Could not unmarshal json", err)
	}

	// combine structs to one
	out := Output{
		DAY_ENERGY:   commonStruct.Body.Data.DAY_ENERGY.Value,
		YEAR_ENERGY:  commonStruct.Body.Data.YEAR_ENERGY.Value,
		TOTAL_ENERGY: commonStruct.Body.Data.TOTAL_ENERGY.Value,
		PAC:          commonStruct.Body.Data.PAC.Value,
		FAC:          commonStruct.Body.Data.FAC.Value,
		IAC:          commonStruct.Body.Data.IAC.Value,
		IDC:          commonStruct.Body.Data.IDC.Value,
		UAC:          commonStruct.Body.Data.UAC.Value,
		UDC:          commonStruct.Body.Data.UDC.Value,
		IAC_L1:       threePhaseStruct.Body.Data.IAC_L1.Value,
		IAC_L2:       threePhaseStruct.Body.Data.IAC_L2.Value,
		IAC_L3:       threePhaseStruct.Body.Data.IAC_L3.Value,
		UAC_L1:       threePhaseStruct.Body.Data.UAC_L1.Value,
		UAC_L2:       threePhaseStruct.Body.Data.UAC_L2.Value,
		UAC_L3:       threePhaseStruct.Body.Data.UAC_L3.Value,
	}

	// Check for empty/faulty read and skip sending
	if out.TOTAL_ENERGY == 0 {
		return
	}

	mqttJson, err := json.Marshal(out)
	if err != nil {
		log.Fatalln("Could not marshal three phase json from struct", err.Error())
	}

	fmt.Println("JSON data:")
	fmt.Println(string(mqttJson))

	mqttClient.Publish(c.MqttTopic, 0, false, mqttJson)
}
