package main

type conf struct {
	InverterIP    string  `yaml:"inverterIP"`
	QueryInterval int     `yaml:"query_interval_minutes"`
	Latitude      float64 `yaml:"latitude"`
	Longitude     float64 `yaml:"longitude"`
	MqttBroker    string  `yaml:"mqtt_broker"`
	MqttPort      int16   `yaml:"mqtt_port"`
	MqttUser      string  `yaml:"mqtt_user"`
	MqttPw        string  `yaml:"mqtt_password"`
	MqttTopic     string  `yaml:"mqtt_topic"`
}

type Common struct {
	Body struct {
		Data struct {
			DAY_ENERGY struct {
				Value float64
			}
			FAC struct {
				Value float32
			}
			IAC struct {
				Value float32
			}
			IDC struct {
				Value float32
			}
			PAC struct {
				Value int
			}
			TOTAL_ENERGY struct {
				Value float64
			}
			UAC struct {
				Value float32
			}
			UDC struct {
				Value float32
			}
			YEAR_ENERGY struct {
				Value float64
			}
		}
	}
}

type ThreePhase struct {
	Body struct {
		Data struct {
			IAC_L1 struct {
				Value float32
			}
			IAC_L2 struct {
				Value float32
			}
			IAC_L3 struct {
				Value float32
			}
			UAC_L1 struct {
				Value float32
			}
			UAC_L2 struct {
				Value float32
			}
			UAC_L3 struct {
				Value float32
			}
		}
	}
}

type Output struct {
	TOTAL_ENERGY, DAY_ENERGY, YEAR_ENERGY                                   float64
	FAC, IAC, IDC, UAC, UDC, IAC_L1, IAC_L2, IAC_L3, UAC_L1, UAC_L2, UAC_L3 float32
	PAC                                                                     int
}
