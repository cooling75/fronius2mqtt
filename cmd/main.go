package main

import (
	"log"
	"os"
	"time"

	"github.com/nathan-osman/go-sunrise"

	"gopkg.in/yaml.v2"
)

func (c *conf) getConfig() *conf {
	configFile, fileError := os.ReadFile("config.yml")
	if fileError != nil {
		log.Fatalln(fileError.Error())
	}

	err := yaml.Unmarshal(configFile, c)
	if err != nil {
		log.Fatalln(err.Error())
	}

	return c
}

func main() {
	var rise, set time.Time
	var c conf

	// Get config
	c.getConfig()

	if len(os.Args[1:]) > 0 && os.Args[1] == "-d" {
		debugConfig(c)
	}

	// Get timezone
	tz := os.Getenv(("TZ"))
	loc, tzerr := time.LoadLocation(tz)
	if tzerr != nil {
		log.Fatalln(tzerr.Error())
	}
	time.Local = loc

	// Create mqtt client
	mqtt := connectMqtt(c.MqttBroker, c.MqttPort, c.MqttUser, c.MqttPw)

	// Start loop
	for {
		rise, set = sunrise.SunriseSunset(c.Latitude, c.Longitude, time.Now().Year(), time.Now().Month(), time.Now().Day())
		if time.Now().In(loc).UnixMilli() > rise.In(loc).Add(-30*time.Minute).UnixMilli() && time.Now().In(loc).UnixMilli() < set.In(loc).Add(30*time.Minute).UnixMilli() {
			fetchAndSend(c, mqtt)
		} else {
			log.Println("Night time shutdown, will sleep 5m until 30m before sunrise for selected location.")
		}
		time.Sleep(5 * time.Minute)
	}
}
