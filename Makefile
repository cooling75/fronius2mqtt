build-amd64:
	# GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ./bin/amd64/fronius2mqtt ./cmd/main.go ./cmd/mqtt.go ./cmd/debug.go ./cmd/fetchAndSend.go ./cmd/structs.go
	docker buildx build --push --platform linux/amd64 --tag "$(IMAGE):$(TAG)-amd64" .

build-arm:
	# GOOS=linux GOARM=7 GOARCH=arm CGO_ENABLED=0 go build -o ./bin/arm/fronius2mqtt ./cmd/main.go ./cmd/mqtt.go ./cmd/debug.go ./cmd/fetchAndSend.go ./cmd/structs.go
	docker buildx build --push --platform linux/arm --tag "$(IMAGE):$(TAG)-arm" .

build-arm64:
	# GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -o ./bin/arm64/fronius2mqtt ./cmd/main.go ./cmd/mqtt.go ./cmd/debug.go ./cmd/fetchAndSend.go ./cmd/structs.go
	docker buildx build --push --platform linux/arm64 --tag "$(IMAGE):$(TAG)-arm64" .

build-manifest:
	docker manifest create $(IMAGE):$(TAG) "$(IMAGE):$(TAG)-amd64" "$(IMAGE):$(TAG)-arm"  "$(IMAGE):$(TAG)-arm64"
	docker manifest push $(IMAGE):$(TAG)
